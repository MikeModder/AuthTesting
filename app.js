require('dotenv').config();

const passport = require('passport');
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const path = require('path');

const app = express();
const port = parseInt(process.env.PORT) || 8080;

/* Configure PassportJS */
require('./src/config/passport')(passport);

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'src/views'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());

/* Insert routes here */
app.use('/', require('./src/routes/index'));

app.use((err, req, res, next) => {
    if(err){
        //return res.status(500).render('error/500', { user: req.user });
        console.error(err)
        return res.status(500).send(err);
    }
    next();
});

app.listen(port, () => {
    console.log(`[INFO] Listening on port ${port}`);
    mongoose.connect(process.env.DB_URL);
});