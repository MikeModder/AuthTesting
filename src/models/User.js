const mongoose = require('mongoose');
const uniquePlugin = require('mongoose-unique-validator');
const bcrypt = require('bcrypt');

const schema = mongoose.Schema({
    username: { type: String, unique: true },
    password: String
});

schema.method('checkPass', (pass) => {
    //const valid = await bcrypt.compare(pass, this.password);
    bcrypt.compare(this.password, pass, (err, same) => {
        return same;
    });
});

/*schema.pre('save', (next) => {
    bcrypt.genSalt(10)
        .then(salt => {
            bcrypt.hash(this.password, salt)
                .then(hash => { this.password = hash; next(); });
        })
        .catch(e => { throw e; });
});*/

schema.plugin(uniquePlugin);

module.exports = mongoose.model('User', schema);