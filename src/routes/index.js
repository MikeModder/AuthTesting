const router = require('express').Router();
const passport = require('passport');
const validator = require('validator');
const bcrypt = require('bcrypt');
const User = require('../models/User');

router.get('/', (req, res) => {
    res.render('home', { user: req.user });
});

router.get('/login', (req, res) => {
    if(req.user) return res.redirect('/');
    res.render('login');
});

router.get('/signup', (req, res) => {
    if(req.user) return res.redirect('/');
    res.render('signup');
});

router.post('/login', passport.authenticate('local', { failureRedirect: '/login' }), (req, res) => {
    res.redirect('/');
});

router.post('/signup', async (req, res) => {
    if(req.user) return res.redirect('/user');
    const username = req.body.username;
    const password = req.body.password;
    const password2 = req.body.password2;
    if(validator.isEmpty(username) || validator.isEmpty(password) || validator.isEmpty(password2)) return res.render('signup', { err: 'One or more fields is empty!' });
    if(password !== password2) return res.render('signup', { err: 'Password and confirmation must match!' });
    const hashedPw = await bcrypt.hash(password, 10);
    const u = new User({ username, password: hashedPw });
    u.save()
        .then(newUsr => {
            req.login(newUsr, (err) => { if(err) throw err; });
            res.redirect('/');
        })
        .catch(e => { throw e; });
});

router.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/');
});

module.exports = router;